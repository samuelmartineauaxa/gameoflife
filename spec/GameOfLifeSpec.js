describe("GameOfLife", function () {

    beforeEach(function () {

    });

    describe("when cell is alive", function () {
        it("should die when live neighbours are fewer than two", function () {
            var initialState = [
                [false, false, false],
                [false, true, false],
                [false, false, false]
            ];


            expect(computeGameOfLifeStep(initialState)[1][1]).toEqual(false);
        });

        it("should die when live neighbours are more than three", function () {
            var initialState = [
                [false, true, false],
                [true, true, true],
                [false, true, false]
            ];

            expect(computeGameOfLifeStep(initialState)[1][1]).toEqual(false);
        });

        it("should live when live neighbours are exactly two or three", function () {
            var initialState = [
                [false, true, false],
                [true, true, false],
                [false, false, false]
            ];

            expect(computeGameOfLifeStep(initialState)[1][1]).toEqual(true);
        });
    });

    describe("when cell is dead", function () {
        it("should live when live neighbours are exactly three", function () {
            var initialState = [
                [false, true, false],
                [true, false, false],
                [false, true, false]
            ];

            expect(computeGameOfLifeStep(initialState)[1][1]).toEqual(true);
        });
    });


});
