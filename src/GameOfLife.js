/*
 * input: array of array of booleans
 * min grid size = 3*3
 * Cell states:
 * true: cell is alive
 * false: cell is dead
 *
 * output: array of array of booleans
 * State after one step
 *
 * */

function computeGameOfLifeStep(grid) {
    var result,
        maxX = grid.length,
        maxY = grid[0].length,
        x, y, line, cell, liveNeighboursCount;

    displayGrid(grid);

    result = initGrid(maxX, maxY);

    for (x = 0; x < maxX; x++) {
        line = grid[x];
        for (y = 0; y < maxY; y++) {
            cell = line[y];
            liveNeighboursCount = countLiveNeighbours(x, y, grid);

            if (cell) {
                result[x][y] = (liveNeighboursCount >= 2 && liveNeighboursCount <= 3);
            } else {
                result[x][y] = (liveNeighboursCount === 3);
            }
        }
    }

    displayGrid(result);
    return result;
}

function initGrid(x, y) {
    var result = [];

    for (var i = 0; i < x; i++) {
        result[i] = [];
        for (var j = 0; j < y; j++) {
            result[i][j] = false;
        }
    }

    return result;
}

function countLiveNeighbours(x, y, grid) {

    var rowSize = grid[0].length,
        columnSize = grid.length,
        subGridRowMin = (y - 1 > -1) ? y - 1 : 0,
        subGridRowMax = (y + 1 < rowSize) ? y + 1 : rowSize-1 ,
        subGridColumnMin = (x - 1 > -1) ? x - 1 : 0,
        subGridColumnMax = (x + 1 < columnSize ) ? x + 1 : columnSize-1 ,
        result = 0;

    for (var j = subGridRowMin; j <= subGridRowMax; j += 1) {
        for (var i = subGridColumnMin; i <= subGridColumnMax; i += 1) {
            if (i !== x || j !== y) {
                if (grid[i][j]) {
                    result++;
                }
            }
        }
    }

    return result;

}

function displayGrid(grid) {
    var maxX = grid.length,
        maxY = grid[0].length,
        output = '';

    for (var i = 0; i < maxX; i++) {
        for (var j = 0; j < maxY; j++) {
            output += grid[i][j] ? '*' : '.';
        }
        output += '\n';
    }
}